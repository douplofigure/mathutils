
all: test spline

test: samples/test.cpp
	g++ -o test -I./include/ samples/test.cpp

spline: samples/spline.cpp
	g++ -o spline -I./include/ samples/spline.cpp
