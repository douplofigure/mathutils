#include <stdio.h>
#include <stdlib.h>

#include <chrono>
#include <iostream>

#include <mathutils/vector.h>
#include <mathutils/matrix.h>
#include <mathutils/integral.h>

#define BM_SIZE (1 << 16)
#define EQ_COUNT 4096

double testFunction(double x) {
  return x*2 + 3;
}

int main(int argc, char ** argv) {

  unsigned long bm_size = BM_SIZE;

  if (argc >= 2) {
    bm_size = atol(argv[1]);
  }
  
  std::cout << "Computing " << bm_size << " (4x4) Matrix-Matrix operations" << std::endl;
  
  float m1Data[16] = {
    1, 0, 7, 3,
    0, 32, 5, 2,
    0, 0, 1, 0,
    9, 0, 0, 1
  };
  
  float m2Data[16] = {
    1, 0, 0, 0,
    0, 0, 1, 0,
    0, 1, 0, 0,
    0, 0, 0, 1
  };
  
  Math::Matrix<4,4,float> A(m1Data);
  Math::Matrix<4,4,float> B(m2Data);
  
  //std::vector<Math::Vector<4, float>> vecs(BM_SIZE);
  
  auto startTime = std::chrono::high_resolution_clock::now();
  Math::Matrix<4,4,float> tmp;
  for (unsigned long int i = 0; i < bm_size; ++i) {
    tmp = A * B;//Math::Vector<4,float>(1, i * i, 4, 32 * i);
  }
  //std::vector<Math::Vector<4, float>> resData = A.solve(vecs);
  auto endTime = std::chrono::high_resolution_clock::now();
  
  double duration = std::chrono::duration<double, std::chrono::seconds::period>(endTime - startTime).count();
  
  std::cout << "Math::Matrix<4,4,float> : " << duration << " s  (" << (duration / (double) bm_size) << " s / equation )" << std::endl;

  startTime = std::chrono::high_resolution_clock::now();
  Math::Vector<4,float> v = A.solve(Math::Vector<4, float>(1,2,3,4));
  endTime = std::chrono::high_resolution_clock::now();

  duration = std::chrono::duration<double, std::chrono::seconds::period>(endTime - startTime).count();
  
  std::cout << "Solving 1 linear (4x4) equation " << duration << " s" << std::endl;

  std::vector<Math::Vector<4,float>> vecs(EQ_COUNT);
  for (unsigned int i = 0; i < EQ_COUNT; ++i) {
    vecs[i] = Math::Vector<4,float>(i, 2*i, i*i, i+1);
  }

  startTime = std::chrono::high_resolution_clock::now();
  std::vector<Math::Vector<4,float>> vs = A.solve(vecs);
  endTime = std::chrono::high_resolution_clock::now();

  duration = std::chrono::duration<double, std::chrono::seconds::period>(endTime - startTime).count();
  
  std::cout << "Solving 1024 linear (4x4) equations " << duration << " s  (" << (duration/EQ_COUNT) << "s / equation)" << std::endl;


  double intValue = Math::integral<double, double>(0.0, 1.0, 80, testFunction);
  std::cout << "Integral value: " << intValue << std::endl;
  
}
