#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <cmath>

#include <mathutils/spline.h>

#define VAL_COUNT 13
#define SAMPLE_COUNT 1024

double f(double x) {
  
  return sin(M_PI * cos(x)) * cos(x);
  
}

int main(int argc, char ** argv) {

  std::vector<double> xVals(VAL_COUNT);
  std::vector<double> yVals(VAL_COUNT);

  for (unsigned int i = 0; i < VAL_COUNT; ++i) {

    xVals[i] = (2 * M_PI / VAL_COUNT) * i;
    yVals[i] = f(xVals[i]);

  }

  Math::Spline<double> spline(xVals, yVals);

  std::cout << "x;y;f(x);delta" << std::endl;
  
  for (unsigned int i = 0; i < SAMPLE_COUNT; ++i) {

    double x = (2 * M_PI / SAMPLE_COUNT) * i;
    double y = spline(x);
    std::cout << x << ";" << y << ";"<< f(x) << ";" << (y - f(x)) << std::endl;
    
  }
  
}
