set terminal postscript eps enhanced color font 'Helvetica,10'
set output 'plot_spline.eps'
set datafile separator ';'
set key autotitle columnhead
plot 'spline.csv' using 1:2 with lines title columnhead, '' using 1:3 with lines title columnhead, '' using 1:4 with lines title columnhead
