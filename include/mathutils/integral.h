#ifndef _INTEGRAL_H
#define _INTEGRAL_H

#include <math.h>
#include <functional>

namespace Math {

template <typename T=double, typename R=double> void gauleg(T x1, T x2, T * x, R * w, const unsigned int n) {

    T eps = 3e-14;
    int i, j, m;
    T p1, p2, p3, pp, xl, xm, z, z1;

    m = (n+1)/2;
    xm = 0.5 * (x2 + x1);
    xl = 0.5 * (x2 - x1);

    for (i = 1; i <= m; ++i) {

        z = cos(M_PI * (i-0.25) / (n+0.5));

label:
        p1 = 1;
        p2 = 0;
        for (j = 1; j <= n; ++j) {

            p3 = p2;
            p2 = p1;
            p1 = ((2 * j - 1) * z * p2 - (j-1)*p3) / j;

        }

        pp = n*(z*p1 - p2) / (z*z - 1);
        z1 = z;
        z = z1-p1/pp;
        if (abs(z-z1) > eps) goto label;

        x[i-1] = xm - xl * z;
        x[n-i] = xm + xl * z;

        w[i-1] = 2.0 * xl / ((1.0 - z*z) * pp * pp);
        w[n-i] = w[i-1];


    }

}

template <typename T=double, typename R=double> inline R integral(T a, T b, unsigned int sampleCount, std::function<R(T)> f) {

    std::vector<T> xValues(sampleCount);
    std::vector<R> wValues(sampleCount);

    gauleg(a, b, xValues.data(), wValues.data(), sampleCount);

    T h = (b - a) / (sampleCount-1);

    R r = (R) 0;

    for (unsigned int i = 0; i < sampleCount; ++i) {

        T x = xValues[i];

        r = r + wValues[i] * f(x);

    }

    return r;

}

}

#endif
