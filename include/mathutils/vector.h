#ifndef VECTOR_H
#define VECTOR_H

#include <cstdarg>
#include <cmath>
#include <ostream>
#include <iostream>

#include "math_exception.h"

#ifdef __SSE2__

#include <x86intrin.h>
#include <immintrin.h>

#endif

namespace Math {

  template<unsigned int dim, typename T = double> class Vector {

  public:
    /**This will copy data, you can free it after the call*/
    Vector(const T * data){
      for (unsigned int i = 0; i < dim; ++i) {
	this->data[i] = data ? data[i] : (T) 0;
      }
    }

    template <unsigned int l> Vector(Vector<l, T> vec) {
      for (unsigned int i = 0; i < dim; ++i) {
	this->data[i] = (i < l) ? vec[i] : (T) 0;
      }
    }

    Vector(std::vector<T> data) {

      if (data.size() != dim) {
	throw Math::trace_exception("Wrong dimension in initializer");
      }

      for (unsigned int i = 0; i < dim; ++i) {
	this->data[i] = data[i];
      }

    }

    Vector() {
      for (unsigned int i = 0; i < dim; ++i)
	this->data[i] = (T) 0;
    }

    Vector(T v) {
      for (unsigned int i = 0; i < dim; ++i)
	this->data[i] = (T) v;
    }

    /*Vector(T t0, ...) {

      std::cout << "Creating vector from initialiser list" << std::endl;

      this->data[0] = t0;
      va_list vl;
      va_start(vl, t0);
      for (int i = 1; i < dim; ++i) {
      std::cout << "Loading argument " << i << std::endl;
      T tmp = va_arg(vl, T);
      this->data[i] = tmp;
      }
      va_end(vl);

      std::cout << "Done" << std::endl;

      }*/

    virtual ~Vector() {

    }

    T& operator[](unsigned int i) {
      if (i < 0 || i >= dim)
	throw trace_exception("No such coordinate");
      return data[i];
    }

    const T& operator()(unsigned int i) const {
      if (i < 0 || i >= dim)
	throw trace_exception("No such coordinate");
      return data[i];
    }

    T operator*(const Vector<dim, T> vec) const {
      T val = (T) 0;
      for (unsigned int i = 0; i < dim; ++i) {
	val = val + this->data[i] * vec.data[i];
      }
      return val;
    }

    Vector<dim, T> operator+(const Vector<dim, T> & vec) const {

      T tmp[dim];
      for (unsigned int i = 0; i < dim; ++i) {
	tmp[i] = data[i] + vec.data[i];
      }
      return Vector<dim, T>(tmp);

    }

    Vector<dim, T> operator+(const double & d) const {
      T tmp[dim];
      for (unsigned int i = 0; i < dim; ++i) {
	tmp[i] = data[i] + d;
      }
      return Vector<dim, T>(tmp);
    }

    Vector<dim, T> operator*(const T v) const {

      T tmp[dim];
      for (unsigned int i = 0; i < dim; ++i) {
	tmp[i] = data[i] *v;
      }
      return Vector<dim, T>(tmp);

    }

    Vector<dim, T> operator-(const Vector<dim, T> vec) const {

      T tmp[dim];
      for (unsigned int i = 0; i < dim; ++i) {
	tmp[i] = data[i] - vec.data[i];
      }
      return Vector<dim, T>(tmp);

    }

    Vector<dim, T> operator/(const T v) const {

      T tmp[dim];
      for (unsigned int i = 0; i < dim; ++i) {
	tmp[i] = data[i] / v;
      }
      return Vector<dim, T>(tmp);

    }

    void normalize() {
      T val = this->length();
      for (unsigned int i = 0; i < dim; ++i) {
	this->data[i] /= val;
      }
    }

    T length() {
      T sum = (T) 0;
      for (unsigned int i = 0; i < dim; ++i) {
	sum += this->data[i] * this->data[i];
      }
      return (T) sqrt(sum);
    }

    const T * getData() {
      return data;
    }



  protected:

  private:

    T data[dim];

  };

  template <unsigned int dim, typename T> inline Vector<dim, T> compMultiply(const Vector<dim, T> & a, const Vector<dim, T> & b) {
    
    std::vector<T> data(dim);
    
    for (unsigned int i = 0; i < dim; ++i) {
      data[i] = a(i) * b(i);
    }
    
    return Vector<dim, T>(data);
  }

  template <unsigned int dim, typename T> inline Vector<dim, T> compDivide(const Vector<dim, T> & a, const Vector<dim, T> & b) {
    
    T data[dim];
    
    for (unsigned int i = 0; i < dim; ++i) {
      data[i] = a(i) / b(i);
    }
    
    return Vector<dim, T>(data);
  }
  

#ifdef __SSE2__

template <> class Vector<4, float> {

    public:

        Vector() : Vector(0,0,0,0) {

        }

        template <unsigned int l> Vector(Vector<l, float> vec) {

            alignas(16) float data[4];

            for (unsigned int i = 0; i < 4; ++i) {
                data[i] = (i < l) ? vec[i] : 0;
            }
            vData = _mm_load_ps(data);
        }

        Vector(float x, float y, float z, float w) {

            float data[4];

            data[0] = x;
            data[1] = y;
            data[2] = z;
            data[3] = w;

            vData = _mm_load_ps(data);

        }

        Vector(std::vector<float> data) {

            if (data.size() != 4) {
                throw Math::trace_exception("Wrong dimension in initializer");
            }

            vData = _mm_load_ps(data.data());

        }

        Vector(const float * data) {

            alignas(16) float tmp[4];

            tmp[0] = data[0];
            tmp[1] = data[1];
            tmp[2] = data[2];
            tmp[3] = data[3];

            vData = _mm_load_ps(tmp);

        }

        Vector(const __m128 vData) {
            this->vData = vData;
        }

        /*const float * getData() {
            return data;
        }*/

        __m128 getVData() {
            return vData;
        }

        const float operator[](int i) const {
            if (i < 0 || i >= 4)
                throw trace_exception("No such coordinate");

            float data[4];
            _mm_store_ps(data, vData);
            return data[i];
        }

        float operator*(Vector<4, float> vec) const {

            /*__m128 v0 = _mm_mul_ps(vData, vec.vData);
            v0 = _mm_add_ps(_mm_shuffle_ps(v0, v0, _MM_SHUFFLE(2, 3, 0, 1)), vec.vData);
            v0 = _mm_add_ps(v0, _mm_shuffle_ps(v0, v0, _MM_SHUFFLE(0, 1, 2, 3)));

            float tmp[4];

            _mm_store_ps(tmp, v0);

            return tmp[0];*/

            alignas(16) float a[4];
            alignas(16) float b[4];

            _mm_store_ps(a, vData);
            _mm_store_ps(b, vec.vData);

            return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];

        }

        Vector<4, float> operator+(Vector<4, float> vec) const {

            return Vector<4, float>(_mm_add_ps(vData, vec.vData));

        }

        Vector<4, float> operator*(float v) const {

            __m128 vv = _mm_load_ps1(&v);

            return Vector<4, float>(_mm_mul_ps(vData, vv));

        }

        Vector<4, float> operator-(Vector<4, float> vec) const {

            return Vector<4, float>(_mm_sub_ps(vData, vec.vData));

        }

        Vector<4, float> operator/(float v) const {

            __m128 vv = _mm_load_ps1(&v);
            return Vector<4, float>(_mm_div_ps(vData, vv));

        }

        void normalize() {
            float val = this->length();
            __m128 vv = _mm_load_ps1(&val);
            this->vData = _mm_div_ps(vData, vv);
        }

        float length() {

            __m128 tmp = _mm_mul_ps(vData, vData);

            float data[4];
            _mm_store_ps(data, tmp);

            return (float) sqrt(data[0] + data[1] + data[2] + data[3]);
        }

    private:
        //float data[4];
        __m128 vData;

};

  template <> class Vector<3, float> {

  public:

    Vector() : Vector(0,0,0) {

    }

    template <unsigned int l> Vector(Vector<l, float> vec) {

      alignas(16) float data[4];

      for (unsigned int i = 0; i < 4; ++i) {
	data[i] = (i < l) ? vec[i] : 0;
      }
      vData = _mm_load_ps(data);
    }

    Vector(float x, float y, float z) {

      float data[4];

      data[0] = x;
      data[1] = y;
      data[2] = z;
      data[3] = 0;

      vData = _mm_load_ps(data);

    }

    /*Vector(std::vector<float> data) {

      if (data.size() != 3) {
	throw Math::trace_exception("Wrong dimension in initializer");
      }

      data.push_back(0);

      vData = _mm_load_ps(data.data());

      }*/

    Vector(const float * data) {

      alignas(16) float tmp[4];

      tmp[0] = data[0];
      tmp[1] = data[1];
      tmp[2] = data[2];
      tmp[3] = 0;

      vData = _mm_load_ps(tmp);

    }

    Vector(const __m128 vData) {
      this->vData = vData;
    }

    Vector(const float v) {
      alignas(16) float tmp[4];

      tmp[0] = v;
      tmp[1] = v;
      tmp[2] = v;
      tmp[3] = v;

      vData = _mm_load_ps(tmp);
      
    }

    /*const float * getData() {
      return data;
      }*/

    __m128 getVData() const {
      return vData;
    }

    const float & operator() (int i) const {
      float * data = (float *) &this->vData;
      return data[i];
    }
    
    float & operator[](int i) {
      if (i < 0 || i >= 3)
	throw trace_exception("No such coordinate");

      //float data[4];
      //_mm_store_ps(data, vData);
      float * data = (float *) &this->vData;
      return  data[i];
      //return data[i];
    }

    float operator*(Vector<3, float> vec) const {

      /*__m128 v0 = _mm_mul_ps(vData, vec.vData);
	v0 = _mm_add_ps(_mm_shuffle_ps(v0, v0, _MM_SHUFFLE(2, 3, 0, 1)), vec.vData);
	v0 = _mm_add_ps(v0, _mm_shuffle_ps(v0, v0, _MM_SHUFFLE(0, 1, 2, 3)));

	float tmp[4];

	_mm_store_ps(tmp, v0);

	return tmp[0];*/

      alignas(16) float a[4];
      alignas(16) float b[4];

      _mm_store_ps(a, vData);
      _mm_store_ps(b, vec.vData);

      return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];// + a[3] * b[3];

    }

    Vector<3, float> operator+(Vector<3, float> vec) const {

      return Vector<3, float>(_mm_add_ps(vData, vec.vData));

    }

    Vector<3, float> operator*(float v) const {

      __m128 vv = _mm_load_ps1(&v);

      return Vector<3, float>(_mm_mul_ps(vData, vv));

    }

    Vector<3, float> operator-(Vector<3, float> vec) const {

      return Vector<3, float>(_mm_sub_ps(vData, vec.vData));

    }

    Vector<3, float> operator/(float v) const {

      __m128 vv = _mm_load_ps1(&v);
      return Vector<3, float>(_mm_div_ps(vData, vv));

    }

    void normalize() {
      float val = this->length();
      __m128 vv = _mm_load_ps1(&val);
      this->vData = _mm_div_ps(vData, vv);
    }

    float length() {

      __m128 tmp = _mm_mul_ps(vData, vData);

      float data[4];
      _mm_store_ps(data, tmp);

      return (float) sqrt(data[0] + data[1] + data[2]);
    }


  private:
    //float data[4];
    __m128 vData;

  };

  template <> inline Vector<3, float> compMultiply<3, float>(const Vector<3, float> & v1, const Vector<3, float> & v2) {

    return Vector<3, float>(_mm_mul_ps(v1.getVData(), v2.getVData()));

  }

#endif

  template <typename T> Vector<3, T> cross(Vector<3, T> vec1, Vector<3, T> vec2) {

    T tmp[3] = {vec1[1] * vec2[2] - vec1[2] * vec2[1], -vec1[2] * vec2[0] + vec1[0] * vec2[2], vec1[0] * vec2[1] - vec1[1] * vec2[0]};

    return Vector<3, T>(tmp);

  }



}

template <unsigned int dim, typename T> inline Math::Vector<dim, T> operator*(T a, Math::Vector<dim, T> vec) {
    T tmp[dim];
    for (unsigned int i = 0; i < dim; ++i) {
        tmp[i] = vec[i] * a;
    }
    return Math::Vector<dim, T>(tmp);
}

template <unsigned int dim, typename T> inline std::ostream& operator<<(std::ostream& stream, Math::Vector<dim, T> vec) {
    stream << "(";
    stream << vec[0];
    for (unsigned int i = 1; i < dim; ++i) {
        stream << ", " << vec[i];
    }
    stream << ")";
    return stream;
}

template <unsigned int dim, typename T> Math::Vector<dim, T> operator/(const double & d, const Math::Vector<dim, T> & vec) {

  T tmp[dim];
  for (unsigned int i = 0; i < dim; ++i) {
    tmp[i] = d / vec(i);
  }

  return Math::Vector<dim, T>(tmp);
  
}

template <unsigned int dim, typename T> inline Math::Vector<dim, T> operator*(int a, Math::Vector<dim, T> vec) {
  return ((T)a) * vec;
}

#endif // VECTOR_H
