#ifndef _VECTORSPLINE_H
#define _VECTORSPLINE_h

#include <vector>
#include "mathutils/vector.h"

namespace Math {

  template <unsigned int dim, typename T> class VectorSpline {
  public:
    VectorSpline(std::vector<double> xValues, std::vector<Vector<dim, T>> yValues) {

      int n = xValues.size();
      this->y2 = std::vector<Vector<dim, T>>(n);
      std::vector<Vector<dim, T>> u(n);

      for (int i = 1; i < n-1; ++i) {

	double sig = (xValues[i]-xValues[i-1]) / (xValues[i+1]-xValues[i-1]);
	Vector<dim, T> p = sig * y2[i-1]+2.0;
	y2[i] = (sig-1) / p;
	u[i] = compDivide((6*((yValues[i+1]-yValues[i])/(xValues[i+1]-xValues[i])-(yValues[i]-yValues[i-1])/(xValues[i]-xValues[i-1]))/(xValues[i+1]-xValues[i-1])-sig*u[i-1]),p);

      }

      y2[n-1] = 0.0;
      for (int i = n-2; i > 0; --i) {
	y2[i] = compMultiply(y2[i], y2[i+1]) + u[i];
      }

      this->xValues = xValues;
      this->yValues = yValues;

    }

    VectorSpline() {

    }
    
    virtual ~VectorSpline() {

    }

    Vector<dim, T> operator() (double x) const {

      int klo = 0;
      int khi = this->xValues.size()-1;

      while (khi - klo > 1) {
	int k = (khi+klo)/2;
	if (xValues[k] > x)
	  khi = k;
	else {

	  if (klo == k) khi--;
	  klo = k;

	}
      }

      double h = xValues[khi]-xValues[klo];
      if (h == 0) {
	//throw runtime_error("Identical xValues in spline");
      }

      double a = (xValues[khi]-x)/h;
      double b = (x-xValues[klo])/h;
      
      return a * yValues[klo] + b * yValues[khi] + ((a*a*a-a)*y2[klo]+(b*b*b-b)*y2[khi])*(h * h)/6.0;

    }

  protected:

  private:

    std::vector<double> xValues;
    std::vector<Vector<dim, T>> yValues;
    std::vector<Vector<dim, T>> y2;

    T pow(T x, int n) const {
      if (n == 1) return x;
      return x * pow(x, n-1);
    }
    
    Vector<dim, T> pow(Vector<dim, T> x, int n) const {

      if (n == 1) return x;
      return x * pow(x, n-1);

    }


  };
  
}

#endif
